#!/bin/bash
nvm install --lts
nvm use --lts
npm install --save-dev workbox-sw workbox-build workbox-core workbox-window workbox-precaching workbox-routing workbox-strategies workbox-expiration workbox-cacheable-response workbox-navigation-preload workbox-cli workbox-runtime-caching
npm install fs-extra workbox-sw --global
npm install workbox-cli --global